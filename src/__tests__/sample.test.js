import { sayHello } from '../sample';

describe('Sample', () => {
  test('Hello Name', () => {
    expect(sayHello('ClearTax')).toBe('Hello ClearTax');
  });
});