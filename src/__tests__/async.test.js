

// jest.mock('../api');

import { getUsers } from '../async';

// jest.mock('../async', () => {
//   return {
//     getUsers: () => Promise.resolve([{ name: "Leanne G" }])
//   }
// });

describe('Async', () => {
  test('getUsers', () => {
    return getUsers()
      .then((users) => {
        expect(users[0].name).toBe('Leanne Graham');
      });
  });

  test('using resolves', () => {
    return expect(getUsers().then((users) => users[0].name)).resolves.toBe('Leanne Graham');
  });

  test('using async & await', async () => {
    const users = await getUsers();
    expect(users[0].name).toBe('Leanne Graham');
  });
});
