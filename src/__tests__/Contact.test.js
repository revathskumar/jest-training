import React from 'react';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

import Contact from '../Contact';

const saveContactMock = jest.fn();

describe('<Contact />', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<Contact saveContact={saveContactMock} />)
  });

  test('render without error', () => {
    wrapper = renderer.create(<Contact saveContact={saveContactMock} />);
    expect(wrapper.toJSON()).toMatchSnapshot();
  });

  test('onChange should update the state', () => {
    wrapper.find('input[type="text"]').simulate('change', { target: { name: 'name', value: 'Peter' } })
    expect(wrapper.state().name).toBe('Peter');

    wrapper.find('input[type="number"]').simulate('change', { target: { name: 'age', value: '30' } })
    expect(wrapper.state().age).toBe("30");
  });

  test('onSubmit should call saveContact', () => {
    wrapper.setState({ name: 'P', age: '3' });
    wrapper.find('form').simulate('submit');

    expect(saveContactMock).toBeCalled();
    expect(saveContactMock).toBeCalledWith({ name: 'P', age: '3' })
  });

  test('make sure Child is rendered', () => {
    expect(wrapper.find('Child')).toHaveLength(1);
  });
});
