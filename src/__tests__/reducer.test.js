import reducer from '../actions';

describe('reducer', () => {
  test('initial state', () => {
    expect(reducer(undefined, {})).toEqual({ loading: false, users: [] });
  });

  test('user loaded', () => {
    expect(reducer(undefined, { type: 'USERS_LOADED', users: ['A'] })).toEqual({ loading: false, users: ['A'] });
  });

  test("loading", () => {
    expect(reducer(undefined, { type: 'LOADING', status: true })).toEqual({ loading: true, users: [] });
  })
})
