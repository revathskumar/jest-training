jest.mock('../api');

import configMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { getUsers, stopLoading } from '../actions';

const mockStore = configMockStore([thunk]);

describe('actions', () => {
  test('simple', () => {
    const store = mockStore({ users: [], loading: false });

    expect(stopLoading()).toEqual({
      type: 'LOADING',
      status: false,
    })
  })

  test('async getUsers', () => {
    const store = mockStore({ users: [] });
    return store.dispatch(getUsers())
      .then(() => {
        expect(store.getActions()).toEqual([{
          type: 'USERS_LOADED',
          users: [{ name: 'Leanne' }]
        }]);
      });
  });
});
