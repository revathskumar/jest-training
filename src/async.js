import { fetchUsers } from './api';

export const getUsers = () => {

  return fetchUsers()
    .then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    })
    .then(function (users) {
      return users;
    });
} 
