import React, { Component } from 'react';

const Child = () => {
  return <div> This is from Child </div>;
}

export default class Contact extends Component {
  constructor() {
    super();

    this.state = {
      name: "",
      age: "",
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  handleSubmit(e) {
    e && e.preventDefault();

    this.props.saveContact(this.state);
  }

  render() {
    return (
      <div className="contact">
        <form onSubmit={this.handleSubmit}>
          <div>
            <label for="name">Name : </label>
            <input type="text" placeholder="Name" id="name" onChange={this.handleChange} />
          </div>
          <div>
            <label for="name">Age : </label>
            <input type="number" placeholder="Age" step="1" id="age" onChange={this.handleChange} />
          </div>
          <div>
            <button type="submit"> Save </button>
          </div>
          <Child />
        </form>
      </div>
    );
  }
}
