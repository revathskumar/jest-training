import { fetchUsers } from './api';

const reducer = (state = { users: [], loading: false }, action) => {
  switch (action.type) {
    case 'USERS_LOADED':
      return Object.assign({}, state, { users: action.users });
    case 'LOADING':
      return Object.assign({}, state, { loading: action.status });
    default:
      return state;
  }
}

export const getUsers = () => (dispatch) => {
  return fetchUsers()
    .then(function (response) {
      if (response.status >= 400) {
        throw new Error("Bad response from server");
      }
      return response.json();
    })
    .then(function (users) {
      dispatch({
        type: 'USERS_LOADED',
        users
      });
    });
}

export const stopLoading = () => {
  return {
    type: 'LOADING',
    status: false
  };
}

export default reducer;
